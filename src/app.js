define([
    'resources/ivds',
    'resources/nodes',
    'text!config/settings.json',
    'text!resources/skins.json',
    'resources/skins',
    'js/overlays',
    'text!config/playerOptions.json'
],
function(ivds, nodes, settings, skinsJSON, skinsJS, overlays, playerOptions) {

    'use strict';

    /// Used for Audio Tutorial
    var alternateSoundtrack = "http://d3425luerwqydx.cloudfront.net/hackathon/Death_Note.mp3";
    var audioPluginConfig = {
        "sounds": [
            {
                "id": "soundtrack",
                "src": "https://d3425luerwqydx.cloudfront.net/hackathon/Death_Note.mp3",
                "volume": 1.0,
                "fadeOutDuration": 3
            }
        ],
        "videoVolume": 0
    };



    // app.js variables
    // hold a reference to the Interlude Player
    var player;
    var head = 'intro';
    settings = JSON.parse(settings);

    // function onNodeStart() {
    //     console.log('node has started');
    // }

    // project essentials
    return {

        playerOptions: JSON.parse(playerOptions),

        settings: settings,

        // Will be called by Helix and is responsible to preare the player
        // for playback of the app (e.g. adding nodes, GUI, etc')
        onPlayerInit: function(ctx) {
            player = ctx.player;

            // nodes
            player.repository.add(nodes);


            // decisions
            for (var i = 0; i < nodes.length; i++) {
                if (nodes[i].data && nodes[i].data.decision) {
                    player.decision.add(nodes[i].id);
                }
            }

            // Graph - for controlbar
            player.graph.setHead(head);

            // RtsPreviewPlugin mapping
            
            // GUI
            overlays.init(player);

            player.append(head);

        },

        // Will be called automatically at the start of the headnode.
        onStarted: function() {
            
            /////// Decision Example
            player.currentNode.once('timeupdate:' + player.currentNode.data.decision.startTime, function() {
                overlays.show();    
            });

            player.currentNode.once('timeupdate:' + player.currentNode.data.decision.endTime, function() {
                overlays.hide();
            });

            

            ////// Seek Example
            player.currentNode.on('timeupdate:' + 1.8, function() {
                player.seek(head);
            });
            

            ////// RTS Example
            var counter = 1;
            function changeChannel() {
                player.switch(counter % 2);
                counter++;
                player.currentNode.once('timeupdate:' + counter, changeChannel);
            }

            player.repository.get('rts').once('timeupdate:' + 1, function() {
                changeChannel();
            });
        },

        // Will be called by Helix once the project is about to reach its end.
        // Responsible for cleanup.
        onEnd: function() {
            // do something...
            player.off(null, null, settings.id);
        },

        onLoad: function(ctx) {
            ctx.playerOptions.plugins.decision.decider = function(parent, children, options) {
                if (parent.metadata && parent.metadata.type === 'rts' && (Object.keys(parent.metadata.indexMap).length > 1 ||
                (parent.metadata.altLength && parent.metadata.altLength > 1))) {
                    if (options.channelsDefaults && options.channelsDefaults[player.currentChannel]) {
                        return options.channelsDefaults[player.currentChannel];
                    }
                    return false;
                }
                return undefined;
            };

            return ctx.when.promise(function(resolve, reject) {
                ctx.loadJS(['stylesheets/main.css'], {
                    success: function() {
                        resolve();
                    },
                    error: function() {
                        reject();
                    }
                });
            });

        }
    };

});