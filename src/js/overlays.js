define(function(ctx) {
    return {
        init: function() {
            // brunette button
            var brunetteBtn = document.createElement('div');
            brunetteBtn.innerHTML = 'brunette';
            brunetteBtn.classList.add('selectBtn');
            // blonde button
            var blondeBtn = document.createElement('div');
            blondeBtn.innerHTML = 'blonde';
            blondeBtn.classList.add('selectBtn');

            var myGuiContainer = document.createElement('div');
            myGuiContainer.classList.add('buttonContainer');
            myGuiContainer.appendChild(blondeBtn);
            myGuiContainer.appendChild(brunetteBtn);

            player.overlays.add('myOverlay', myGuiContainer, { visible:false });

            blondeBtn.addEventListener('click', function() {
                player.append('blonde');
                player.overlays.hide('myOverlay');
            });

            brunetteBtn.addEventListener('click', function() {
                player.append('brunette');
                player.overlays.hide('myOverlay');
            });
        },

        show: function() {
            player.overlays.show('myOverlay');
        },

        hide: function() {
            player.overlays.hide('myOverlay');
        }
    };
});