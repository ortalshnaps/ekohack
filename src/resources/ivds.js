define([       'text!resources/ivd/video_b2_1otgr9rkk04k8gokkg0kwkss8gws0.ivd'
        ,
        'text!resources/ivd/video_b1_1otgrajex2g0ogcgko4w88s0ggw44.ivd'
        ,
        'text!resources/ivd/video_a1_1otgrakytuxw4g0sso0kg4ossk04o.ivd'
        ,
        'text!resources/ivd/video_9e01435adf2ee4e00ea2bc65fb8c779f_1otgrdrjm8e80o8ggk04osogwk4gc.ivd'
    ]     ,
    function(    vid_video_b2_1otgr9rkk04k8gokkg0kwkss8gws0 
    ,  
        vid_video_b1_1otgrajex2g0ogcgko4w88s0ggw44 
    ,  
        vid_video_a1_1otgrakytuxw4g0sso0kg4ossk04o 
    ,  
        vid_video_9e01435adf2ee4e00ea2bc65fb8c779f_1otgrdrjm8e80o8ggk04osogwk4gc 
     
    ) {
    var ivds = {
                    vid_video_b2_1otgr9rkk04k8gokkg0kwkss8gws0: vid_video_b2_1otgr9rkk04k8gokkg0kwkss8gws0 
            ,  
                    vid_video_b1_1otgrajex2g0ogcgko4w88s0ggw44: vid_video_b1_1otgrajex2g0ogcgko4w88s0ggw44 
            ,  
                    vid_video_a1_1otgrakytuxw4g0sso0kg4ossk04o: vid_video_a1_1otgrakytuxw4g0sso0kg4ossk04o 
            ,  
                    vid_video_9e01435adf2ee4e00ea2bc65fb8c779f_1otgrdrjm8e80o8ggk04osogwk4gc: vid_video_9e01435adf2ee4e00ea2bc65fb8c779f_1otgrdrjm8e80o8ggk04osogwk4gc 
             
            };
    return ivds;
});